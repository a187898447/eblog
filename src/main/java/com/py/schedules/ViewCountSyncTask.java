package com.py.schedules;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.py.entity.Post;
import com.py.service.IPostService;
import com.py.util.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Component
public class ViewCountSyncTask {
    @Autowired
    RedisUtil redisUtil;
    @Autowired
    RedisTemplate redis;
    @Autowired
    IPostService postService;

//    @Scheduled(cron = "0 0 0 * * ?")    //每天零点同步
//    @Scheduled(cron = "0/5 * * * * *")    //每五秒更新
    public void task(){
        Set<String> keys = redis.keys("rank:post:*");
        List<String> ids = new ArrayList<>();   //所有在redis储存了的博客id
        for(String key : keys){
            String postId = key.substring("rank:post:".length());
            if(redisUtil.hHasKey(key, "post:viewCount")){
                ids.add(postId);
            }
        }
        if(ids.isEmpty()) return;

        List<Post> posts = postService.list(new QueryWrapper<Post>().in("id", ids));    //根据储存的id查出的博客信息
        for(Post post : posts){
            String key = "rank:post:"+post.getId();
            Integer viewCount = Integer.parseInt(redisUtil.hget(key,"post:viewCount").toString());
            post.setViewCount(viewCount);   //将数据库的阅读量信息更新
        }
        postService.updateBatchById(posts); //批量更新
    }

}
