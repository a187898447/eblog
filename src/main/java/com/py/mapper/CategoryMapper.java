package com.py.mapper;

import com.py.entity.Category;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author py
 * @since 2021-08-01
 */
public interface CategoryMapper extends BaseMapper<Category> {

}
