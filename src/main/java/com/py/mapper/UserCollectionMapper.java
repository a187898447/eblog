package com.py.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.py.entity.Post;
import com.py.entity.UserCollection;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.py.vo.PostVo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author py
 * @since 2021-08-01
 */
public interface UserCollectionMapper extends BaseMapper<UserCollection> {

    @Select("select p.*,c.created as collectionCreated from m_post p left join m_user_collection c on p.id = c.post_id "+
            "${ew.customSqlSegment} ")
    public IPage<PostVo> paging(Page page, @Param(Constants.WRAPPER) QueryWrapper<Post> wrapper);

}
