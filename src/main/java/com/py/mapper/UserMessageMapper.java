package com.py.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.py.entity.UserMessage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.py.vo.UserMessageVo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author py
 * @since 2021-08-01
 */
public interface UserMessageMapper extends BaseMapper<UserMessage> {

    @Select("select m.*,u.username as formUserName,p.title as postTitle,c.content as commentContent from m_user_message m " +
            " left join m_user u on u.id = m.from_user_id " +
            " left join m_post p on p.id = m.post_id " +
            " left join m_comment c on c.id = m.comment_id " +
            " ${ew.customSqlSegment} ")
    IPage<UserMessageVo> selectMessage(Page page, @Param(Constants.WRAPPER) QueryWrapper<UserMessage> wrapper);
}
