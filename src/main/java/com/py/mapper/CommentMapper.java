package com.py.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.py.entity.Comment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.py.entity.Post;
import com.py.vo.CommentVo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author py
 * @since 2021-08-01
 */
public interface CommentMapper extends BaseMapper<Comment> {

    @Select("select c.*,u.id as authorId,u.username as authorName,u.avatar as authorAvatar" +
            ",(select username from m_user where id = c.parent_id) as formUserName " +
            "from m_comment c left join m_user u on c.user_id = u.id " +
            "${ew.customSqlSegment} ")
    IPage<CommentVo> selectComment(Page page,@Param(Constants.WRAPPER) QueryWrapper<Comment> wrapper);

    @Select("select c.*,p.title as postTitle " +
            "from m_comment c left join m_post p on c.post_id = p.id " +
            "${ew.customSqlSegment} ")
    IPage<CommentVo> queryComment(Page page,@Param(Constants.WRAPPER) QueryWrapper<Comment> wrapper);

}
