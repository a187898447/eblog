package com.py.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.py.entity.Post;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.py.vo.PostVo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author py
 * @since 2021-08-01
 */
public interface PostMapper extends BaseMapper<Post> {

    @Select("select p.*,u.id as authorId,u.username as authorName,u.avatar as authorAvatar,c.name as categoryName " +
            "from m_post p left join m_user u on p.user_id = u.id left join m_category c on p.category_id = c.id " +
            "${ew.customSqlSegment} ")  //这种写法可以直接将条件构造器的内容传入sql语句中
    IPage<PostVo> selectPages(Page page, @Param(Constants.WRAPPER) QueryWrapper<Post> wrapper);

    @Select("select p.*,u.id as authorId,u.username as authorName,u.avatar as authorAvatar,c.name as categoryName " +
            "from m_post p left join m_user u on p.user_id = u.id left join m_category c on p.category_id = c.id " +
            "${ew.customSqlSegment} ")
    PostVo selectOnePost(@Param(Constants.WRAPPER) QueryWrapper<Post> wrapper);
}
