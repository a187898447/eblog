package com.py.common.aspect.annotation;

import java.lang.annotation.*;

/**
 * @program: vue-blog
 * @description: 限制密码长度的注解
 * @author: Lori
 * @create: 2022-04-21 16:51
 **/
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface LimitLength {

    int minLength() default 1;

    int maxLength() default 1;
}
