package com.py.common.aspect;

import cn.hutool.core.lang.Assert;
import com.py.common.aspect.annotation.LimitLength;
import com.py.entity.User;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * @program: vue-blog
 * @description: 这是针对限制密码长度的aop
 * @author: Lori
 * @create: 2022-04-21 16:51
 **/
@Aspect
@Component
public class LimitLengthAspect {

    /**
     * 切面，这里使用的是前置通知,切点在LimitLength注解上
     *
     * @author Lori
     * @date 2022-04-21 17:07
     * @param point 切点指定的类
     * @return
     */
    @Before("@annotation(com.py.common.aspect.annotation.LimitLength)")
    public void checkBefore(JoinPoint point)  {
        // 获取注解
        MethodSignature signature = (MethodSignature) point.getSignature();
        Method method = signature.getMethod();
        LimitLength annotation = method.getDeclaredAnnotation(LimitLength.class);
        // 获取实体
        Object[] args = point.getArgs();
        for (Object arg : args) {
           if (arg instanceof User) {
               String password = ((User) arg).getPassword();
               Assert.isTrue(password.length() >= annotation.minLength() && password.length() <= annotation.maxLength(), "长度不符合标准！");
           }
        }
    }

}
