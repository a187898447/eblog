package com.py.common.aspect;

import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSONUtil;
import com.py.common.aspect.annotation.LimitLength;
import com.py.common.aspect.annotation.ReturnData;
import com.py.common.lang.Result;
import com.py.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.lang.annotation.Retention;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * @program: vue-blog
 * @description: 这是针对限制密码长度的aop
 * @author: Lori
 * @create: 2022-04-21 16:51
 **/
@Aspect
@Component
@Slf4j
public class AroundAspect {

    /**
     * 切面,这里的切点设置在result上，这里使用的是环绕通知
     *
     * @author Lori
     * @date 2022-04-24 09:54
     * @param point 切点指定的类
     * @return
     */
    @Around("execution(!static com.py.common.lang.Result *(..))")
    public void checkAround(ProceedingJoinPoint point) throws Throwable {
        log.info("----入参：----\n {}", JSONUtil.toJsonStr(point.getArgs()));
        Object proceed = point.proceed();
        if (proceed instanceof  Result) {
            Object data = ((Result) proceed).getData();
            if (ObjectUtil.isNotNull(data)) {
                Field[] fields = data.getClass().getDeclaredFields();
                for (Field field : fields) {
                    ReturnData annotation = field.getAnnotation(ReturnData.class);
                    if (ObjectUtil.isNotNull(annotation)) {
                        field.setAccessible(true);
                        field.set(data, "123456");
                    }
                }
            }
        }
//        Object proceed = point.proceed();
//        if (proceed instanceof  Result) {
//            if (ObjectUtil.isNull(((Result) proceed).getData())) {
//                ((Result) proceed).setData(" ");
//            }
//        }
        log.info("----返回：----\n {}", proceed);
    }

}
