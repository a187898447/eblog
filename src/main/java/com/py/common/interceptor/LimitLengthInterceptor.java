package com.py.common.interceptor;

import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.ObjectUtil;
import com.py.common.aspect.annotation.LimitLength;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

/**
 * @program: eblog
 * @description: 限制长度的拦截器，用于拦截属性上的长度限制注解
 * @author: Lori
 * @create: 2022-04-22 10:47
 **/
@Component
public class LimitLengthInterceptor implements HandlerInterceptor {

    /**
     * 在进入方法前执行的拦截器
     *
     * @author Lori
     * @date 2022-04-22 11:43
     * @param request request
     * @param response response
     * @param handler handler
     * @return 是否通行
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        // 获取接口
        HandlerMethod handlerMethod = (HandlerMethod) handler;
        Method method = handlerMethod.getMethod();
        // 获取所有参数的类型，这里处理的是注解在属性上时的情况
        Class<?>[] parameterTypes = method.getParameterTypes();
        for (Class<?> parameterType : parameterTypes) {
            // 字段
            Field[] fields = parameterType.getDeclaredFields();
            for (Field field : fields) {
                LimitLength annotation = field.getAnnotation(LimitLength.class);
                if (ObjectUtil.isNotEmpty(annotation)) {
                    // 如果存在这个注解，则直接从入参里获取这个param
                    String parameter = request.getParameter(field.getName());
                    if (ObjectUtil.isEmpty(parameter)) {
                        parameter = "";
                    }
                    Assert.isTrue(parameter.length() >= annotation.minLength() && parameter.length() <= annotation.maxLength(), "长度有误！");
                }
            }
        }
        return true;
    }

}
