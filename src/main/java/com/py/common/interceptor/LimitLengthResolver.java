package com.py.common.interceptor;

import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.ObjectUtil;
import com.py.common.aspect.annotation.LimitLength;
import org.springframework.core.MethodParameter;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

/**
 * @program: eblog
 * @description: 处理类
 * @author: Lori
 * @create: 2022-04-25 09:15
 **/
@Component
public class LimitLengthResolver implements HandlerMethodArgumentResolver {


    @Override
    public boolean supportsParameter(MethodParameter methodParameter) {
        // 获取所有参数的类型，这里处理的是注解在属性上时的情况
        Class<?> parameterType = methodParameter.getParameterType();
        Field[] fields = parameterType.getDeclaredFields();
        for (Field field : fields) {
            LimitLength annotation = field.getDeclaredAnnotation(LimitLength.class);
            if (ObjectUtil.isNotEmpty(annotation)) {
                return true;
            }
        }

        // 获取所有参数，这里处理的是注解在参数上时的情况
        Parameter methodParam = methodParameter.getParameter();
        LimitLength annotation = methodParam.getDeclaredAnnotation(LimitLength.class);
        ResponseBody responseBody = methodParam.getDeclaredAnnotation(ResponseBody.class);
        if (ObjectUtil.isNotEmpty(annotation) && ObjectUtil.isNull(responseBody)) {
            return true;
        }
        return false;
    }

    /**
     *处理器，这里是在处理参数、属性上的LimitLength注解
     *
     * @author Lori
     * @date 2022-04-26 10:44
     * @param methodParameter methodParameter
     * @param modelAndViewContainer modelAndViewContainer
     * @param nativeWebRequest nativeWebRequest
     * @param webDataBinderFactory webDataBinderFactory
     * @return  参数
     */
    @Override
    public Object resolveArgument(MethodParameter methodParameter, ModelAndViewContainer modelAndViewContainer, NativeWebRequest nativeWebRequest, WebDataBinderFactory webDataBinderFactory) throws Exception {
        HttpServletRequest request = nativeWebRequest.getNativeRequest(HttpServletRequest.class);
        // HttpServletResponse response = nativeWebRequest.getNativeResponse(HttpServletResponse.class);

        // 获取所有参数的类型，这里处理的是注解在属性上时的情况
        Class<?> parameterType = methodParameter.getParameterType();
        // 字段
        Field[] fields = parameterType.getDeclaredFields();
        for (Field field : fields) {
            LimitLength annotation = field.getAnnotation(LimitLength.class);
            if (ObjectUtil.isNotEmpty(annotation)) {
                // 如果存在这个注解，则直接从入参里获取这个param
                String param = request.getParameter(field.getName());
                if (ObjectUtil.isEmpty(param)) {
                    param = "";
                }
                Assert.isTrue(param.length() >= annotation.minLength() && param.length() <= annotation.maxLength(), "长度有误！");
                return param;
            }
        }

        // 获取所有参数，这里处理的是注解在参数上时的情况
        Parameter methodParam = methodParameter.getParameter();
        LimitLength annotation = methodParam.getDeclaredAnnotation(LimitLength.class);
        if (ObjectUtil.isNotEmpty(annotation)) {
            String param = request.getParameter(methodParam.getName());
            // 如果存在这个注解，则直接从入参里获取这个param
            if (ObjectUtil.isEmpty(param)) {
                // 如果拿不到，先看看则判断是不是带了ResponseBody，带了则直接抛出
                ResponseBody methodAnnotation = methodParameter.getMethodAnnotation(ResponseBody.class);
                Assert.isNull(methodAnnotation, "LimitLength注解仅能限制单个的参数长度，请将注解加在属性上!");
                param = "";
            }
            Assert.isTrue(param.length() >= annotation.minLength() && param.length() <= annotation.maxLength(), "长度有误！");
            return param;
        }
        return null;
    }
}
