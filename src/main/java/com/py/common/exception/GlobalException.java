package com.py.common.exception;

import cn.hutool.json.JSONUtil;
import com.py.common.lang.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
@ControllerAdvice   //全局异常捕获
public class GlobalException {

    @ExceptionHandler(value = Exception.class)
    public ModelAndView handler(HttpServletRequest request, HttpServletResponse response,Exception e) throws IOException {
        log.error(e.getMessage());
        String header = request.getHeader("X-Requested-with");
        if(header!=null && "XMLHttpRequest".equals(header)){
            response.setContentType("application/json;charset=utf-8");
            response.getWriter().print(JSONUtil.toJsonStr(Result.fail(e.getMessage())));    //ajax的话在这已经返回
            return null;
        }
        //如果设置文件名为error.html，springboot会默认在所有报错时都跳转进入，所以这里命名为tips
        ModelAndView mv = new ModelAndView("tips");
        mv.addObject("message",e.getMessage());
        return mv;
    }

    @ExceptionHandler(value = NullPointerException.class)
    public ModelAndView handler(HttpServletRequest request, HttpServletResponse response,NullPointerException e) throws IOException {
        log.error(e.getMessage());
        String header = request.getHeader("X-Requested-with");
        if(header!=null && "XMLHttpRequest".equals(header)){
            response.setContentType("application/json;charset=utf-8");
            response.getWriter().print(JSONUtil.toJsonStr(Result.fail("程序内部问题，请联系管理员!")));    //ajax的话在这已经返回
            return null;
        }
        //如果设置文件名为error.html，springboot会默认在所有报错时都跳转进入，所以这里命名为tips
        ModelAndView mv = new ModelAndView("tips");
        mv.addObject("message","程序内部问题，请联系管理员!");
        return mv;
    }

}
