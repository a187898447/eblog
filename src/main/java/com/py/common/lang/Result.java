package com.py.common.lang;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/*
    自定义返回值类型
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Result implements Serializable {
    private int status;   //0为正常 非0为不正常
    private String msg;
    private Object data;
    private String action;

    public Result(int status, String msg, Object data) {
        this.status = status;
        this.msg = msg;
        this.data = data;
    }

    public static Result success(Object data) {
        return new Result(0, "成功", data);
    }

    public static Result success(Object data,String action) {
        return new Result(0, "成功", data,action);
    }

    public static Result fail(String msg) {
        return new Result(1, msg, null);
    }
}
