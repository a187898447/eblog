package com.py.config;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.py.entity.Category;
import com.py.service.ICategoryService;
import com.py.service.IPostService;
import com.py.util.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.context.ServletContextAware;

import javax.servlet.ServletContext;
import java.util.List;
import java.util.Map;
import java.util.Set;

/*
    这里会在运行时触发，将导航栏部分内容查找出添加到servletContext内
 */
@Component
public class ContextStartUp implements ApplicationRunner , ServletContextAware {

    @Autowired
    ICategoryService categoryService;
    @Autowired
    IPostService postService;
    @Autowired
    RedisUtil redis;

    ServletContext servletContext;  //储存在服务器，随本次服务而生成、销毁    因为这些信息在本次服务内基本都不会变化

    @Override
    public void run(ApplicationArguments args) throws Exception {
        List<Category> category = categoryService.list(new QueryWrapper<Category>().eq("status", 0));
        servletContext.setAttribute("category",category);
        postService.initWeekRank();
        List<Map> hotPost = postService.getWeekRank();
        servletContext.setAttribute("hots",hotPost);
    }

    @Override
    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }
}
