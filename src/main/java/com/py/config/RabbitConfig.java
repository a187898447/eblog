package com.py.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfig {

    public final static String QUEUE = "es_queue";
    public final static String EXCHANGE = "es_exchange";
    public final static String BINDINGKEY= "es_binding_key";

    @Bean
    public Queue exQueue(){
        return new Queue(QUEUE);
    }

    @Bean
    DirectExchange exchange(){
        return new DirectExchange(EXCHANGE);
    }

    @Bean
    Binding binding(Queue exQueue,DirectExchange exchange){
        return BindingBuilder.bind(exQueue).to(exchange).with(BINDINGKEY);
    }
}
