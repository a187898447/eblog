package com.py.config;

import com.py.common.interceptor.LimitLengthInterceptor;
import com.py.common.interceptor.LimitLengthResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

@Configuration
public class SpringConfig implements WebMvcConfigurer {

    @Value("${file.upload.dir}")
    private String uploadDir;

    @Autowired
    private LimitLengthInterceptor limitLengthInterceptor;
    @Autowired
    private LimitLengthResolver limitLengthResolver;

//    @Override
//    public void addInterceptors(InterceptorRegistry registry) {
//        //addPathPatterns用于添加拦截路径
//        //excludePathPatterns用于添加不拦截的路径
//        registry.addInterceptor(limitLengthInterceptor).addPathPatterns("/*");
//    }

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        argumentResolvers.add(limitLengthResolver);
    }

    //添加静态资源映射
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        //表示文件路径，所有/upload/avatar/开头的请求 都会去后面配置的路径下查找资源
        registry.addResourceHandler("/upload/**")
                // 其中三斜杠本意是双斜杠，还有一个转义字符
                .addResourceLocations("file:///" + uploadDir + "/");
    }
}
