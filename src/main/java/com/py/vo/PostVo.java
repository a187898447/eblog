package com.py.vo;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.py.entity.Post;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PostVo extends Post {

    /*
        用户id
     */
    private Long authorId;
    /*
        用户名
     */
    private String authorName;
    /*
        用户头像
     */
    private String authorAvatar;
    /*
        分类名
     */
    private String categoryName;
    /*
        时间差（用于展示）
     */
    private String timeAgo;

    /*
        收藏时间
     */
    private String collectionCreated;
}
