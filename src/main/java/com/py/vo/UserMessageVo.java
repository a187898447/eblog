package com.py.vo;

import com.py.entity.UserMessage;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserMessageVo extends UserMessage {
    private String toUserName;
    private String formUserName;
    private String postTitle;
    private String commentContent;
    /*
        时间差（用于展示）
     */
    private String timeAgo;
}
