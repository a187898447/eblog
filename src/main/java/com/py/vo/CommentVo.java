package com.py.vo;

import com.py.entity.Comment;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommentVo extends Comment {
    /*
        用户id
     */
    private Long authorId;
    /*
        用户名
     */
    private String authorName;
    /*
        用户名
     */
    private String formUserName;
    /*
        用户头像
     */
    private String authorAvatar;
    /*
        博客的标题
     */
    private String postTitle;
    /*
        时间差（用于展示）
     */
    private String timeAgo;

}
