package com.py.controller;

import cn.hutool.core.lang.Assert;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.py.common.lang.Result;
import com.py.entity.Post;
import com.py.entity.UserCollection;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.time.LocalDateTime;
import java.util.Map;

//用户控制器
@Controller
@RequestMapping("/collection")
public class CollectionController extends BaseController {

    //判断本博客是否被收藏
    @ResponseBody
    @RequestMapping("/find")
    public Result CollectionFind(Long cid){
        UserCollection collection = collectionService.getOne(new QueryWrapper<UserCollection>()
                .eq("post_id", cid)
                .eq("user_id", getProFile().getId())
        );
        return Result.success(ObjectUtil.isNotNull(collection));
    }

    @ResponseBody
    @RequestMapping("add")
    public Result add(Long cid){
        Post post = postService.getById(cid);
        Assert.isTrue(post.getStatus()==0,"本帖已被删除！");
        int count = collectionService.count(new QueryWrapper<UserCollection>()
                .eq("post_id", cid)
                .eq("user_id", getProFile().getId())
        );
        if(count>0){
            Result.fail("本帖已被收藏!");
        }
        UserCollection userCollection = new UserCollection();
        userCollection.setUserId(getProFile().getId());
        userCollection.setPostId(cid);
        userCollection.setPostUserId(post.getUserId());
        userCollection.setCreated(LocalDateTime.now());
        userCollection.setModified(LocalDateTime.now());
        collectionService.save(userCollection);
        return Result.success(null);
    }

    @ResponseBody
    @RequestMapping("remove")
    public Result remove(Long cid){
        Post post = postService.getById(cid);
        Assert.isTrue(post.getStatus()==0,"本帖已被删除！");
        int count = collectionService.count(new QueryWrapper<UserCollection>()
                .eq("post_id", cid)
                .eq("user_id", getProFile().getId())
        );
        if(count==0){
            Result.fail("本帖未被收藏！");
        }
        collectionService.removeById(cid);
        return Result.success(null);
    }


}
