package com.py.controller;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.ShearCaptcha;
import cn.hutool.core.lang.Assert;
import cn.hutool.crypto.SecureUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.py.common.aspect.annotation.LimitLength;
import com.py.common.lang.Result;
import com.py.entity.User;
import com.py.entity.UserMessage;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicReference;

//用户控制器
@Controller
@Slf4j
public class AuthController extends BaseController {

    private static final String CAPTCHA_KEY = "CAPTCHA_KEY";

    /**
     * 线程池
     */
    @Resource
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    /**
     * 异步任务命名（用于控制并行任务启动）
     */
    private static final String[] tasks = {
            "check",
            "securityLogin"
    };

    /**
     * 用于异步时判断验证码是否正确的参数，增加了线程可见性保证不会出错
     */
    volatile Boolean flag = true;

    //验证码
    @RequestMapping("/captcha.jpg")
    public void getCaptcha(HttpServletResponse response) throws IOException {
        ShearCaptcha lineCaptcha = CaptchaUtil.createShearCaptcha(120, 40,4,2);
        BufferedImage image = lineCaptcha.getImage();
        response.setHeader("Cache-Control","no-store,no-cache");
        response.setContentType("image/jpeg");
        request.getSession().setAttribute("CAPTCHA_KEY",lineCaptcha.getCode());
        ServletOutputStream outputStream = response.getOutputStream();
        ImageIO.write(image,"jpg",outputStream);
        if (outputStream!=null) {
            outputStream.close();
        }
    }

    @GetMapping("/login")
    public String login(){
        return "/auth/login";
    }

    @ResponseBody
    @PostMapping("/login")
//    @LimitLength(minLength = 5, maxLength = 10)
    public Result doLogin(@LimitLength(minLength = 5, maxLength = 10) User user, String vercode) {
        String code = (String)request.getSession().getAttribute(CAPTCHA_KEY);
        // 这里使用异步查询，本质上是多开一个线程,两个进程仅消耗一个进程的时间
        CompletableFuture<Void> future = CompletableFuture.allOf(Arrays.stream(tasks).map(c -> CompletableFuture.runAsync(() -> {
            if (tasks[0].equals(c)) {
                flag = check(code, vercode);
                Assert.isTrue(flag, "验证码输入不正确!");
            } 
            if (tasks[1].equals(c)) {
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                   log.warn("线程异常");
                }
                if (flag) {
                    flag = securityLogin(user);
                }
                Assert.isTrue(flag, "登录失败!");
            }
        }, threadPoolTaskExecutor)).toArray(CompletableFuture[]::new));

        // 异步执行
        future.join();
        return Result.success("登录成功！","/index");
    }

    /**
     * 检测验证码
     *
     * @author Lori
     * @date 2022-04-24 14:51
     * @param code 保存的验证码
     * @param vercode 验证码
     * @return result
     */
    public static Boolean check(String code, String vercode) {
        if(vercode==null || !vercode.equals(code)){
            return false;
        }
        return true;
    }

    /**
     * shiro登录
     *
     * @author Lori
     * @date 2022-04-24 14:55
     * @param user 用户信息
     * @return 是否成功
     */
    public static Boolean securityLogin(User user) {
        UsernamePasswordToken token = new UsernamePasswordToken(user.getEmail(), SecureUtil.md5(user.getPassword()));
        // 登录
        SecurityUtils.getSubject().login(token);
        return true;
    }

    @GetMapping("/register")
    public String register(){
        return "/auth/reg";
    }

    @ResponseBody
    @PostMapping("/register")
    public Result doRegister(@Validated User user, String repass, String vercode){
        String code = (String)request.getSession().getAttribute(CAPTCHA_KEY);
        if(repass==null || !user.getPassword().equals(repass)){
            return Result.fail("两次密码不一致!");
        }else if(vercode==null || !vercode.equals(code)){
            return Result.fail("验证码输入不正确!");
        }
        Result result = userService.register(user);

        UserMessage message = new UserMessage();
        message.setToUserId(userService.getOne(new QueryWrapper<User>().eq("email",user.getEmail())).getId());
        message.setContent("欢迎加入！");
        message.setType(0);
        message.setCreated(LocalDateTime.now());
        message.setModified(LocalDateTime.now());
        message.setStatus(0);
        messageService.save(message);

        return result;
    }

    @RequestMapping("/user/logout")
    public String logout(){
        SecurityUtils.getSubject().logout();
        return "redirect:/login";
    }

    @ResponseBody
    @RequestMapping("/test")
    public Result test(){
        User user = new User();
        user.setGender("cnaklsnck");
        return Result.success(user);
    }
}
