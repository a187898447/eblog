package com.py.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.py.entity.Post;
import com.py.search.model.PostDocument;
import com.py.util.TimeAgoUtil;
import com.py.vo.PostVo;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
public class IndexController extends BaseController {

    @RequestMapping({"","/","index"})
    public String index(
            @RequestParam(required = false,defaultValue = "1") int pn,
            @RequestParam(required = false,defaultValue = "2") int size
    ){
        //置顶数据
        IPage<PostVo> topPageData = postService.paging(new Page(1,5),null,null,1,null,"created");
        for(PostVo topData :topPageData.getRecords() ){
            topData.setTimeAgo(TimeAgoUtil.getTimeAgo(topData.getCreated()));
        }
        request.setAttribute("topPageData",topPageData);
        //全部数据
        IPage<PostVo> centrePageData = postService.paging(new Page(pn,size),null,null,null,null,"created");
        for(PostVo centreData :centrePageData.getRecords() ){
            centreData.setTimeAgo(TimeAgoUtil.getTimeAgo(centreData.getCreated()));
        }
        request.setAttribute("centrePageData",centrePageData);
        request.setAttribute("categoryId",0);
        return "index";
    }

    @RequestMapping("/search")
    public ModelAndView search(ModelAndView mv, String q,
                               @RequestParam(required = false,defaultValue = "1") int pn,
                               @RequestParam(required = false,defaultValue = "2") int size
    ){
        IPage<PostDocument> page =  searchService.search(new Page(pn,size),q);
        mv.addObject("q",q);
        mv.addObject("searchPageData",page);
        mv.setViewName("/search");
        return mv;
    }

}
