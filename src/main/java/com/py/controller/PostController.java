package com.py.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.py.common.lang.Result;
import com.py.config.RabbitConfig;
import com.py.entity.*;
import com.py.search.mq.PostMqIndexMessage;
import com.py.util.TimeAgoUtil;
import com.py.util.UploadUtil;
import com.py.vo.CommentVo;
import com.py.vo.PostVo;
import net.sf.jsqlparser.statement.Commit;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

//博客控制器
@Controller
public class PostController extends BaseController {

    @Autowired
    ServletContext servletContext;
    @Autowired
    UploadUtil upload;

    //进入博客分类页
    @RequestMapping("/category/{id:\\d*}")  //在这里指定id必须为数字类型
    public String category(@PathVariable Long id,
                           @RequestParam(required = false,defaultValue = "1") int pn,
                           @RequestParam(required = false,defaultValue = "2") int size
    ){
        IPage<PostVo> centrePageData = postService.paging(new Page(pn,size),id,null,null,null,"created");
        for(PostVo centreData :centrePageData.getRecords() ){
            centreData.setTimeAgo(TimeAgoUtil.getTimeAgo(centreData.getCreated()));
        }
        request.setAttribute("centrePageData",centrePageData);  //分页了的博客
        request.setAttribute("categoryId",id);
        return "/post/category";
    }

    //进入博客详情页
    @RequestMapping("/post/{id:\\d*}")  //在这里指定id必须为数字类型w
    public String detail(@PathVariable Long id,
                         @RequestParam(required = false,defaultValue = "1") int pn,
                         @RequestParam(required = false,defaultValue = "2") int size
    ){
        PostVo post = postService.selectOnePost(new QueryWrapper<Post>().eq("p.id",id));
        Assert.isTrue( post.getStatus()==0,"本篇文章已被删除！");
        post.setTimeAgo(TimeAgoUtil.getTimeAgo(post.getCreated()));
        request.setAttribute("post",post);  //该博客的内容

        postService.setViewCount(post);

        IPage<CommentVo> commentPageData = commentService.paging(new Page(pn,size),post.getId(),null,"created");
        for(CommentVo comment :commentPageData.getRecords() ){
            comment.setTimeAgo(TimeAgoUtil.getTimeAgo(comment.getCreated()));
            //如果已登录，则判断各评论是否被点赞了
            if(SecurityUtils.getSubject().isAuthenticated()){
                int count = actionService.count(new QueryWrapper<UserAction>()
                        .eq("comment_id", comment.getId())
                        .eq("user_id", getProFile().getId())
                );
                if(count>0){
                    comment.setIsOK(true);
                }else{
                    comment.setIsOK(false);
                }
            }
        }
        request.setAttribute("commentPageData",commentPageData);    //分页了的评论

        request.setAttribute("categoryId", post.getCategoryId());

        return "/post/detail";
    }

    @GetMapping("/post/edit")
    public ModelAndView edit(ModelAndView mv,Long id){
        if(id!=null){
            Post post = postService.getById(id);
            Assert.isTrue(post.getStatus()==0,"本篇文章已被删除！");
            Assert.isTrue(getProFile().getId().equals(post.getUserId()),"无权编辑！");
            mv.addObject("post",post);
        }else{
            mv.addObject("post",null);
        }
        mv.setViewName("/post/edit");
        return mv;
    }

    //新增或编辑
    @ResponseBody
    @PostMapping("/post/edit")
    public Result edit(@Validated Post post, String vercode){
        String code = (String)request.getSession().getAttribute("CAPTCHA_KEY");
        if(vercode==null || !vercode.equals(code)){
            return Result.fail("验证码输入不正确!");
        }
        Post temPost = null;
        if(post.getId()==null){ //id为空是新增
            temPost = new Post();
            temPost.setCategoryId(post.getCategoryId());
            temPost.setTitle(post.getTitle());
            temPost.setContent(post.getContent());
            temPost.setUserId(getProFile().getId());
            temPost.setViewCount(0);
            temPost.setRecommend(0);
            temPost.setLevel(0);
            temPost.setStatus(0);
            temPost.setCreated(LocalDateTime.now());
        }else{   //id不为空则是编辑
            temPost = postService.getById(post.getId());
            BeanUtil.copyProperties(post,temPost);
        }
        temPost.setModified(LocalDateTime.now());
        postService.saveOrUpdate(temPost);

        template.convertAndSend(RabbitConfig.EXCHANGE,RabbitConfig.BINDINGKEY,new PostMqIndexMessage(temPost.getId(),PostMqIndexMessage.CREATE_OR_UPDATE));
        return Result.success(null,"/user/index");
    }

    //软删除博客
    @ResponseBody
    @PostMapping("/post/del")
    public Result del(Long id){
        Post post = postService.getById(id);
        Assert.isTrue(post.getStatus()==0,"本篇文章已被删除！");
        Assert.isTrue(getProFile().getId().equals(post.getUserId()),"无权编辑！");
        post.setStatus(1);
        post.setModified(LocalDateTime.now());
        postService.updateById(post);
        template.convertAndSend(RabbitConfig.EXCHANGE,RabbitConfig.BINDINGKEY,new PostMqIndexMessage(id,PostMqIndexMessage.REMOVE));
        return Result.success(null);
    }

    //置顶或加精
    @ResponseBody
    @PostMapping("/post/set")
    public Result set(Long id,int rank,String field){
        Post post = postService.getById(id);
        Assert.isTrue(post.getStatus()==0,"本篇文章已被删除！");
        if("level".equals(field)){
            post.setLevel(rank);
        }else if("recommend".equals(field)){
            post.setRecommend(rank);
        }
        post.setModified(LocalDateTime.now());
        postService.updateById(post);
        return Result.success(null);
    }

    //评论或回复评论
    @ResponseBody
    @Transactional  //涉及到多张表，开启注解
    @PostMapping("/post/reply")
    public Result reply(Long id,String content,Long commentId){
        Post post = postService.getById(id);
        Comment comment = new Comment();
        comment.setContent(content);
        if(commentId!=null){
            comment.setParentId(commentId);
        }
        comment.setPostId(id);
        comment.setUserId(getProFile().getId());
        comment.setVoteUp(0);
        comment.setLevel(0);
        comment.setStatus(0);
        comment.setCreated(LocalDateTime.now());
        comment.setModified(LocalDateTime.now());
        commentService.save(comment);
        //评论数加一
        post.setCommentCount(post.getCommentCount()+1);
        postService.updateById(post);
        //每周热议中评论数+1
        postService.incrCommentCountAndUnionWeekRank(id,true);
        List<Map> hotPost = postService.getWeekRank();
        servletContext.setAttribute("hots",hotPost);

        //新建消息
        UserMessage message = new UserMessage();
        message.setFromUserId(getProFile().getId());
        message.setToUserId(post.getUserId());
        message.setPostId(id);
        message.setContent("博客被回复");
        message.setType(1);
        if(commentId!=null){
            message.setToUserId(commentService.getById(commentId).getUserId());
            message.setCommentId(commentId);
            message.setContent("评论被回复");
            message.setType(2);
        }
        message.setCreated(LocalDateTime.now());

        message.setModified(LocalDateTime.now());
        message.setStatus(0);
        messageService.save(message);

        return Result.success(null,"/post/"+id);
    }

    //上传头像
    @ResponseBody
    @RequestMapping("/post/upload")
    public Result upload(MultipartFile file) throws IOException {
        return upload.upload(UploadUtil.type_post,file);
    }

    //删除评论
    @ResponseBody
    @Transactional
    @PostMapping("/comment/del")
    public Result CommentDel(Long id){
        Comment comment = commentService.getById(id);
        comment.setStatus(1);
        commentService.updateById(comment);

        Post post = postService.getById(comment.getPostId());
        //评论数减一
        post.setCommentCount(post.getCommentCount()+1);
        //每周热议中评论数-1
        postService.incrCommentCountAndUnionWeekRank(post.getId(),false);
        postService.updateById(post);
        return Result.success(null,"/post/"+post.getId());
    }

    //点赞或取消赞
    @ResponseBody
    @Transactional
    @PostMapping("/comment/zan")
    public Result zan(boolean ok,Long id){
        Comment comment = commentService.getById(id);
        if(!ok){
            //无记录，新增记录
            UserAction action = new UserAction();
            action.setUserId(getProFile().getId());
            action.setPostId(comment.getPostId());
            action.setCommentId(id);
            action.setCreated(LocalDateTime.now());
            action.setModified(LocalDateTime.now());
            actionService.save(action);

            comment.setVoteUp(comment.getVoteUp()+1);
        }else{
            //有记录，删除记录
            UserAction action = actionService.getOne(new QueryWrapper<UserAction>().eq("comment_id", id));
            actionService.removeById(action);

            comment.setVoteUp(comment.getVoteUp()-1);
        }
        commentService.updateById(comment);
        return Result.success(null);
    }

}
