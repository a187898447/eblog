package com.py.controller;

import com.py.service.*;
import com.py.shiro.AccountProFile;
import org.apache.shiro.SecurityUtils;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;

public class BaseController {
    @Autowired
    HttpServletRequest request;

    @Autowired
    IPostService postService;
    @Autowired
    ICommentService commentService;
    @Autowired
    IUserCollectionService collectionService;
    @Autowired
    IUserMessageService messageService;
    @Autowired
    IUserService userService;
    @Autowired
    IUserActionService actionService;
    @Autowired
    SearchService searchService;

    @Autowired
    RabbitTemplate template;

    public AccountProFile getProFile(){
        return (AccountProFile) SecurityUtils.getSubject().getPrincipal();
    }
}
