package com.py.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.py.common.lang.Result;
import com.py.vo.PostVo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/admin")
public class AdminController extends BaseController {

    @ResponseBody
    @RequestMapping("initEsData")
    public Result initEsData(){
        int size = 1000;
        Page page = new Page<>();
        for(int i=0;i<1000;i++){
            page.setCurrent(i);
            IPage<PostVo> paging = postService.paging(page, null, null, null, null, null);

            searchService.initEsData(paging.getRecords());

            //当查出的数据不足1000条时，说明没有下一页了，此时退出循环
            if(paging.getRecords().size() < size){
                break;
            }
        }
        return Result.success(null);
    }
}
