package com.py.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.py.common.lang.Result;
import com.py.entity.Post;
import com.py.entity.User;
import com.py.entity.UserCollection;
import com.py.entity.UserMessage;
import com.py.shiro.AccountProFile;
import com.py.util.TimeAgoUtil;
import com.py.util.UploadUtil;
import com.py.vo.CommentVo;
import com.py.vo.PostVo;
import com.py.vo.UserMessageVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Component
@RequestMapping("/user")
public class UserController extends BaseController {

    @Autowired
    UploadUtil upload;

    @GetMapping({"home/{id:\\d*}","home/{name:\\S*}","home"})
    public ModelAndView home(
            @PathVariable(required = false) Long id
            ,@PathVariable(required = false) String name
            , ModelAndView mv
    ){
        User user = null;
        if(id!=null){
            user = userService.getById(id);
        }else if(StrUtil.isNotEmpty(name)){
            user = userService.getOne(new QueryWrapper<User>().eq("username",name));
        }else{
            user= userService.getById(getProFile().getId());
        }
        user.setTimeAgo(TimeAgoUtil.getTimeAgo(user.getCreated()));

        IPage<Post> posts = postService.page(new Page(1, 10), new QueryWrapper<Post>()
                .eq("user_id", user.getId())
                //30天内的文章
                .gt("created", DateUtil.offsetDay(new Date(), -30))
                .orderByDesc("created")
        );
        for(Post post:posts.getRecords()){
            post.setTimeAgo(TimeAgoUtil.getTimeAgo(post.getCreated()));
        }

        IPage<CommentVo> comments = commentService.selectComment(new Page(1, 5),user.getId(),"c.created");
        for(CommentVo comment:comments.getRecords()){
            comment.setTimeAgo(TimeAgoUtil.getTimeAgo(comment.getCreated()));
        }

        mv.addObject("user",user);
        mv.addObject("posts",posts);
        mv.addObject("comments",comments);
        mv.setViewName("/user/home");
        return mv;
    }

    @GetMapping("set")
    public ModelAndView set(ModelAndView mv){
        User user = userService.getById(getProFile().getId());
        mv.addObject("user",user);
        mv.setViewName("/user/set");
        return mv;
    }

    @GetMapping("index")
    public ModelAndView index(ModelAndView mv){
        int publicCount = postService.count(new QueryWrapper<Post>().eq(
                "user_id", getProFile().getId())
                .eq("status",0)
        );
        mv.addObject("publicCount",publicCount);
        int collectionCount = collectionService.count(new QueryWrapper<UserCollection>().eq("user_id",getProFile().getId()));
        mv.addObject("collectionCount",collectionCount);
        mv.setViewName("/user/index");
        return mv;
    }

    @GetMapping("message")
    public ModelAndView message(
            ModelAndView mv,
            @RequestParam(required = false,defaultValue = "1") int pn,
            @RequestParam(required = false,defaultValue = "2") int size){
        IPage<UserMessageVo> pages = messageService.paging(new Page(pn, size), new QueryWrapper<UserMessage>()
                .eq("m.to_user_id", getProFile().getId())
                .eq("m.status", 0)
                .orderByDesc("m.created")
        );
        for(UserMessageVo page : pages.getRecords()){
            page.setTimeAgo(TimeAgoUtil.getTimeAgo(page.getCreated()));
        }
        mv.addObject("messages",pages);
        mv.setViewName("/user/message");
        return mv;
    }

    //更改信息
    @ResponseBody
    @PostMapping("set")
    public Result set(User user){
        User temp = userService.getById(getProFile().getId());
        Assert.notNull(temp.getUsername(),"昵称不能为空！");
        int count = userService.count(new QueryWrapper<User>()
                .eq("username",temp.getUsername())
                .ne("id",temp.getId())
        );
        Assert.isTrue(count==0,"昵称不能重复！");
        temp.setUsername(user.getUsername());
        temp.setGender(user.getGender());
        temp.setSign(user.getSign());
        temp.setModified(LocalDateTime.now());
        userService.updateById(temp);

        AccountProFile proFile = getProFile();
        proFile.setUsername(temp.getUsername());
        return Result.success(null,"/user/home");
    }

    //更新头像信息
    @ResponseBody
    @PostMapping("update")
    public Result update(String avatar){
        User temp = userService.getById(getProFile().getId());
        temp.setAvatar(avatar);
        temp.setModified(LocalDateTime.now());
        userService.updateById(temp);
        AccountProFile proFile = getProFile();
        proFile.setAvatar(avatar);
        return Result.success(null);
    }

    //上传头像
    @ResponseBody
    @RequestMapping("upload")
    public Result upload(MultipartFile file) throws IOException {
        return upload.upload(UploadUtil.type_avatar,file);  //返回的数据包括头像url，然后前端调用update方法
    }

    //更新密码
    @ResponseBody
    @RequestMapping("repass")
    public Result repass(String nowpass,String pass,String repass) {
        User temp = userService.getById(getProFile().getId());
        Assert.isTrue(temp.getPassword().equals(SecureUtil.md5(nowpass)),"旧密码不正确！");
        if(pass==null && repass==null && !pass.equals(repass)){
            return Result.fail("两次输入的密码不一致!");
        }
        temp.setPassword(SecureUtil.md5(pass));
        temp.setModified(LocalDateTime.now());
        userService.updateById(temp);
        return new Result(0,"更新成功！请重新登录！",null,"/user/logout");
    }

    //获取发布的文章
    @ResponseBody
    @RequestMapping("public")
    public Result userPublic(
            @RequestParam(required = false,defaultValue = "1") int pn,
            @RequestParam(required = false,defaultValue = "2") int size
    ){
        IPage page = postService.page(new Page(pn,size),new QueryWrapper<Post>()
                .eq("user_id", getProFile().getId())
                .eq("status",0)
                .orderByDesc("created")
        );
        return Result.success(page);
    }

    //获取收藏的文章
    @ResponseBody
    @RequestMapping("collection")
    public Result collection(
            @RequestParam(required = false,defaultValue = "1") int pn,
            @RequestParam(required = false,defaultValue = "2") int size
    ){
        IPage<PostVo> page = collectionService.paging(new Page(pn, size), getProFile().getId(), "created"); //关联查询
//        IPage page = postService.page(new Page(pn, size), new QueryWrapper<Post>()
//                .inSql("id", "select post_id from m_user_collection where user_id = " + getProFile().getId())
//        );  //直接使用嵌套查询
        return Result.success(page);
    }

    //删除/一键清空消息
    @ResponseBody
    @RequestMapping("remove")
    public Result remove(Long id,@RequestParam(required = false,defaultValue = "false") Boolean all){
        List<UserMessage> ums = messageService.list(new QueryWrapper<UserMessage>()
                .eq("to_user_id", getProFile().getId())
                .eq(!all,"id",id)   //没有传值，默认false，则根据id查
        );
        for(UserMessage um : ums){
            um.setStatus(1);
        }
        messageService.updateBatchById(ums);

        return Result.success(null);
    }

    //获取未读消息的条数
    @ResponseBody
    @RequestMapping("nums")
    public Map nums(){
        int count = messageService.count(new QueryWrapper<UserMessage>()
                .eq("to_user_id", getProFile().getId())
                .eq("status",0)
        );
        Map<String, Integer> build = MapUtil.builder("status", 0).put("count", count).build();
        return build;
    }

}
