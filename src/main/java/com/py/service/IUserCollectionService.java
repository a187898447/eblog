package com.py.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.py.entity.Post;
import com.py.entity.UserCollection;
import com.baomidou.mybatisplus.extension.service.IService;
import com.py.vo.PostVo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author pyv
 * @since 2021-08-01
 */
public interface IUserCollectionService extends IService<UserCollection> {

    public IPage<PostVo> paging(Page page, Long userId, String order);
}
