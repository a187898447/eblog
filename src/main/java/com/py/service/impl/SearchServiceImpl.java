package com.py.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.convert.Convert;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.py.entity.Post;
import com.py.search.model.PostDocument;
import com.py.search.repository.PostRepository;
import com.py.service.IPostService;
import com.py.service.SearchService;
import com.py.vo.PostVo;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.index.query.MultiMatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class SearchServiceImpl implements SearchService {
    @Autowired
    PostRepository postRepository;
    @Autowired
    IPostService postService;

    @Override
    public IPage search(Page page, String keyword) {
        //mp的page改为jpa的page
        Long current =  page.getCurrent()-1;
        Long size =  page.getSize();
        Pageable pageable = PageRequest.of(current.intValue(),size.intValue());

        //搜索
        MultiMatchQueryBuilder builder = QueryBuilders.multiMatchQuery(keyword, "title", "authorName", "categoryName");
        org.springframework.data.domain.Page<PostDocument> search = postRepository.search(builder, pageable);

        //将结果信息转为mp的page
        IPage pageData = new Page(page.getCurrent(),page.getSize(),search.getTotalElements());
        pageData.setRecords(search.getContent());

        return pageData;
    }

    @Override
    public void initEsData(List<PostVo> records) {
        List<PostDocument> postDocuments = new ArrayList<>();
        for(int i=0;i<records.size();i++){
            PostDocument convert = Convert.convert(PostDocument.class, records.get(i));
            postDocuments.add(convert);
        }
        postRepository.saveAll(postDocuments);
    }

    @Override
    public void createorupdate(Long postId) {
        PostVo post = postService.selectOnePost(new QueryWrapper<Post>().eq("p.id",postId));
        Assert.isTrue( post.getStatus()==0,"本篇文章已被删除！");

        PostDocument convert = Convert.convert(PostDocument.class, post);
        postRepository.save(convert);

        log.info("索引"+postId+"已新增或更新");
    }

    @Override
    public void remove(Long postId) {
        postRepository.deleteById(postId);
        log.info("索引"+postId+"已被删除");
    }
}
