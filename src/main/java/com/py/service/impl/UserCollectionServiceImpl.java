package com.py.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.py.entity.Post;
import com.py.entity.UserCollection;
import com.py.mapper.PostMapper;
import com.py.mapper.UserCollectionMapper;
import com.py.service.IUserCollectionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.py.vo.PostVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author py
 * @since 2021-08-01
 */
@Service
public class UserCollectionServiceImpl extends ServiceImpl<UserCollectionMapper, UserCollection> implements IUserCollectionService {

    @Autowired
    UserCollectionMapper userCollection;

    @Override
    public IPage<PostVo> paging(Page page, Long userId, String order) {
        return userCollection.paging(page,new QueryWrapper<Post>()
                .eq(userId!=null,"c.user_id",userId)
                .orderByDesc(order!=null,order)
        );
    }
}
