package com.py.service.impl;

import cn.hutool.crypto.SecureUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.py.common.lang.Result;
import com.py.entity.User;
import com.py.mapper.UserMapper;
import com.py.service.IUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.py.shiro.AccountProFile;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author py
 * @since 2021-08-01
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

    @Override
    public AccountProFile login(String username, String password) {
        User user = this.getOne(new QueryWrapper<User>().eq("email",username));
        if(user == null) {
            throw new UnknownAccountException();
        }
        if(!user.getPassword().equals(password)){
            throw new IncorrectCredentialsException("密码不正确!");
        }
        user.setLasted(LocalDateTime.now());
        this.updateById(user);

        //返回一些可公开的东西
        AccountProFile profile = new AccountProFile();  //封装一个返回对象的类
        BeanUtils.copyProperties(user, profile); //将user中一些相同的属性复制profile
        return profile;
    }

    //注册
    @Override
    public Result register(User user) {
        int count = this.count(new QueryWrapper<User>()
                .eq("email", user.getEmail())
                .or()
                .eq("username", user.getUsername())
        );
        if(count > 0){
            return Result.fail("邮箱或用户名已被注册!");
        }
        User temp = new User();
        temp.setEmail(user.getEmail());
        temp.setUsername(user.getUsername());
        temp.setPassword(SecureUtil.md5(user.getPassword()));

        temp.setCreated(LocalDateTime.now());
        temp.setLasted(LocalDateTime.now());
        temp.setVipLevel(0);
        temp.setGender("0");
        temp.setAvatar("/res/images/avatar/default.png");
        this.save(temp);

        return Result.success("注册成功！","/login");
    }

}
