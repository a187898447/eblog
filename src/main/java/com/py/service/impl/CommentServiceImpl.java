package com.py.service.impl;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.py.entity.Comment;
import com.py.mapper.CommentMapper;
import com.py.service.ICommentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.py.vo.CommentVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author py
 * @since 2021-08-01
 */
@Service
public class CommentServiceImpl extends ServiceImpl<CommentMapper, Comment> implements ICommentService {

    @Autowired
    CommentMapper commentMapper;
    @Override
    public IPage<CommentVo> paging(Page page, Long postId, Long userId, String order) {
        return commentMapper.selectComment(page,new QueryWrapper<Comment>()
                .eq(postId!=null,"post_id",postId)
                .eq(userId!=null,"user_id",userId)
                .eq("c.status",0)
                .orderByDesc(order!=null,order)
        );
    }
    public IPage<CommentVo> selectComment(Page page,Long userId, String order) {
        return commentMapper.queryComment(page,new QueryWrapper<Comment>()
                .eq(userId!=null,"c.user_id",userId)
                .eq("c.status",0)
                .orderByDesc(order!=null,order)
        );
    }
}
