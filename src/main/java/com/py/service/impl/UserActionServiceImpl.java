package com.py.service.impl;

import com.py.entity.UserAction;
import com.py.mapper.UserActionMapper;
import com.py.service.IUserActionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author py
 * @since 2021-08-01
 */
@Service
public class UserActionServiceImpl extends ServiceImpl<UserActionMapper, UserAction> implements IUserActionService {

}
