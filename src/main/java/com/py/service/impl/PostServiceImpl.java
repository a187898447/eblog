package com.py.service.impl;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.py.entity.Post;
import com.py.mapper.PostMapper;
import com.py.service.IPostService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.py.util.RedisUtil;
import com.py.vo.PostVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.stereotype.Service;

import java.time.ZoneId;
import java.util.*;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author py
 * @since 2021-08-01
 */
@Service
public class PostServiceImpl extends ServiceImpl<PostMapper, Post> implements IPostService {
    @Autowired
    PostMapper postMapper;
    @Autowired
    RedisUtil redis;

    @Override
    public IPage<PostVo> paging(Page page, Long categoryId, Long userId, Integer level, Boolean recommend, String order) {
        return postMapper.selectPages(page,new QueryWrapper<Post>()
                .eq(categoryId!=null,"category_id",categoryId)
                .eq("p.status",0)
                .eq(userId!=null,"user_id",userId)
                .eq(level!=null && level==0,"level",0)     //level为0时，查level等于0的数据 即不置顶的数据
                .gt(level!=null && level>0,"level",0)     //level大于0时，查level大于0的数据 即置顶的数据
                .orderByDesc(order!=null,order)
        );
    }

    @Override
    public PostVo selectOnePost(QueryWrapper<Post> wrapper) {
        return postMapper.selectOnePost(wrapper);
    }

    /**
     * 本周热议数据初始化
     */
    @Override
    public void initWeekRank() {
        //获取七天内发布的文章
        List<Post> posts = this.list(new QueryWrapper<Post>()
                .eq("status",0)
                .gt("created", DateUtil.offsetDay(new Date(), -6))   //往前偏移六天(七天包括今天)
                .select("id", "title", "user_id", "comment_count", "view_count", "created")
        );
        //初始化文章的总评论
        for(Post post : posts){
            String key = "day:rank:"+DateUtil.format(post.getCreated(),DatePattern.PURE_DATE_PATTERN);  //格式化
            redis.zSet(key,post.getId(),post.getCommentCount());

            Long expireTime = this.getExpireTime(Date.from( post.getCreated().atZone( ZoneId.systemDefault()).toInstant()));
            redis.expire("key",expireTime);

            //缓存文章的基本信息(id、标题、评论数、作者)
            this.hashCachePostIdAndTitle(post,expireTime);
        }
        //做并集
        this.zunionAndStoreLast7DayForWeekRank();
    }

    //获取每周热议榜
    @Override
    public List<Map> getWeekRank() {
        Set<ZSetOperations.TypedTuple> zSetRanks = redis.getZSetRank("week:rank", 0, 6);
        List<Map> hotPost = new ArrayList<Map>();
        for(ZSetOperations.TypedTuple zSetRank : zSetRanks){
            Map<String,Object> map = new HashMap<String,Object>();
            Object value = zSetRank.getValue();
            String postKey = "rank:post:" + value;
            if((int)redis.hget(postKey,"post:commentCount")>0){
                map.put("id",value);
                map.put("title",redis.hget(postKey,"post:title"));
                map.put("commentCount",redis.hget(postKey,"post:commentCount"));
                map.put("viewCount",redis.hget(postKey,"post:viewCount"));
                hotPost.add(map);
            }
        }
        return hotPost;
    }

    //评论后缓存数据修改
    @Override
    public void incrCommentCountAndUnionWeekRank(long postId,boolean isIns) {
        String currrentKey = "day:rank:"+DateUtil.format(new Date(),DatePattern.PURE_DATE_PATTERN);
        //如果今天的信息没有缓存过，则添加到缓存里
        if(redis.getZSetRank(currrentKey,1,1) != null){
            Post post = this.getById(postId);
            Long expireTime = this.getExpireTime(Date.from( post.getCreated().atZone( ZoneId.systemDefault()).toInstant()));
            this.hashCachePostIdAndTitle(post,expireTime);
        }
        //仅对当天数据操作，如果是新增则+1，如果是删除则-1
        redis.zIncrementScore(currrentKey,postId,isIns?1:-1);
        //重新做并集
        this.zunionAndStoreLast7DayForWeekRank();
    }

    //博客访问量增加
    @Override
    public void setViewCount(PostVo post) {
        String key = "rank:post:"+post.getId();
        String viewCount = String.valueOf(redis.hget(key,"post:viewCount"));
        if(!"null".equals(viewCount)){
            post.setViewCount(Integer.parseInt(viewCount)+1); //如果缓存里有就直接获取并加一
        }else{
            post.setViewCount(post.getViewCount()+1);   //如果缓存里没有就从查出的数据里加一
        }
        redis.hset(key,"post:viewCount",post.getViewCount());   //将最终数据添加到缓存里
    }

    /**
     * 文章每日评论并集获得本周评论数量
     */
    private void zunionAndStoreLast7DayForWeekRank() {
        String currrentKey = "day:rank:"+DateUtil.format(new Date(),DatePattern.PURE_DATE_PATTERN);
        String destKey =  "week:rank";
        List otherKeys = new ArrayList();
        for(int i=-6;i<0;i++){
            otherKeys.add("day:rank:"+DateUtil.format(DateUtil.offsetDay(new Date(), i),DatePattern.PURE_DATE_PATTERN));
        }
        redis.zUnionAndStore(currrentKey,otherKeys,destKey);
    }

    /**
     * 缓存文章基本信息
     * @param post
     * @param expireTime
     */
    private void hashCachePostIdAndTitle(Post post, long expireTime) {
        String key = "rank:post:" + post.getId();
        redis.hset(key,"post:id",post.getId(),expireTime);
        redis.hset(key,"post:title",post.getTitle(),expireTime);
        redis.hset(key,"post:commentCount",post.getCommentCount(),expireTime);
        redis.hset(key,"post:viewCount",post.getViewCount(),expireTime);
    }

    public Long getExpireTime(Date endDate){
        //7天后自动过期
        long between = DateUtil.between(new Date(),endDate,DateUnit.SECOND);    //时间间隔
        long expireTime = 7 * 24 * 60 * 60 - between; //过期时间
        return expireTime;
    }
}
