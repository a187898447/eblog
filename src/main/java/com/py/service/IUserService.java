package com.py.service;

import com.py.common.lang.Result;
import com.py.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;
import com.py.shiro.AccountProFile;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author py
 * @since 2021-08-01
 */
public interface IUserService extends IService<User> {
    AccountProFile login(String username,String password);

    Result register(User user);

}
