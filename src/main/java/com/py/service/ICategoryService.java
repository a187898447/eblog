package com.py.service;

import com.py.entity.Category;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author py
 * @since 2021-08-01
 */
public interface ICategoryService extends IService<Category> {

}
