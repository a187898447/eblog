package com.py.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.py.vo.PostVo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface SearchService {
    IPage search(Page page, String keyword);

    void initEsData(List<PostVo> records);

    void createorupdate(Long postId);

    void remove(Long postId);
}
