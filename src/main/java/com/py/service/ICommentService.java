package com.py.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.py.entity.Comment;
import com.baomidou.mybatisplus.extension.service.IService;
import com.py.vo.CommentVo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author py
 * @since 2021-08-01
 */
public interface ICommentService extends IService<Comment> {

    /**
     * 1.分页
     * 2.文章id
     * 3.用户id
     * 4.排序
     */
    IPage<CommentVo> paging(Page page, Long postId, Long userId, String order);

    public IPage<CommentVo> selectComment(Page page,Long userId, String order);
}
