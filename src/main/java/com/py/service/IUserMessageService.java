package com.py.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.py.entity.UserMessage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.py.vo.UserMessageVo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author py
 * @since 2021-08-01
 */
public interface IUserMessageService extends IService<UserMessage> {

    IPage<UserMessageVo> paging(Page page, QueryWrapper<UserMessage> orderByDesc);
}
