package com.py.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.py.entity.Post;
import com.baomidou.mybatisplus.extension.service.IService;
import com.py.vo.PostVo;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author py
 * @since 2021-08-01
 */
public interface IPostService extends IService<Post> {
    /**
     * 查询所有博客
     * 1.分页信息
     * 2.分类id
     * 3.用户id
     * 4.置顶
     * 5.精选
     * 6.排序
     */
    IPage<PostVo> paging(Page page,Long categoryId,Long userId,Integer level,Boolean recommend,String order);

    //博客详情
    PostVo selectOnePost(QueryWrapper<Post> wrapper);

    void initWeekRank();

    List<Map> getWeekRank();

    void incrCommentCountAndUnionWeekRank(long postId,boolean isIns);

    void setViewCount(PostVo post);
}
