package com.py.shiro;

import cn.hutool.json.JSONUtil;
import com.py.common.lang.Result;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.web.filter.authc.UserFilter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Handler;

public class AuthFilter extends UserFilter {

    @Override
    protected void redirectToLogin(ServletRequest re, ServletResponse rs) throws IOException {
        HttpServletRequest request = (HttpServletRequest) re;
        HttpServletResponse response = (HttpServletResponse)rs;
        //ajax 弹窗提示
        String header = request.getHeader("X-Requested-with");
        if(header!=null && "XMLHttpRequest".equals(header)){
            boolean authenticated = SecurityUtils.getSubject().isAuthenticated();
            if(!authenticated){
                response.setContentType("application/json;charset=utf-8");
                response.getWriter().print(JSONUtil.toJsonStr(Result.fail("请先登录再进行操作!")));
            }
        }else{
            //web 页面跳转
            super.redirectToLogin(request, response);
        }
    }
}
