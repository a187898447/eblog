package com.py.shiro;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.py.entity.User;
import com.py.service.IUserService;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/*AccountProFile
    若token存在，会进入这里
 */
@Component  //注入spring
public class AccountRealm extends AuthorizingRealm {
    @Autowired
    IUserService userService;

    //授权
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
//         1.通过principals获取已经认证完毕的用户名
        AccountProFile profile = (AccountProFile) principalCollection.getPrimaryPrincipal();
//         2.根据用户名去数据库中查询该认证用户下角色/权限信息
        Set<String> roles = new HashSet<String>();

        if("admin".equals(profile.getUsername())){
            roles.add("admin");
        }else{
            roles.add("user");
        }
        // 3.把角色/权限信息封装进SimpleAuthorizationInfo.class
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        info.addRoles(roles);
        // 4.返回SimpleAuthorizationInfo.class
        return info;
    }

    //在这里实际执行登录操作
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        UsernamePasswordToken token = (UsernamePasswordToken) authenticationToken;

        AccountProFile profile = userService.login(token.getUsername(), String.valueOf(token.getPassword()));

        return new SimpleAuthenticationInfo(profile, token.getCredentials(), getName());  //返回身份信息、秘钥信息、realm的名字

    }
}
