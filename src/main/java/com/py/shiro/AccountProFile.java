package com.py.shiro;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Transient;

import java.io.Serializable;
import java.util.Collection;

//储存在shiro中的值
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccountProFile implements Serializable  {

    private Long id;

    private String username;

    /**
     * 头像
     */
    private String avatar;



}
