package com.py.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.py.common.aspect.annotation.LimitLength;
import com.py.common.aspect.annotation.ReturnData;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author py
 * @since 2021-08-01
 */
@TableName("m_user")
public class User extends Model<User> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 昵称
     */
    @NotBlank(message = "昵称不能为空")
    private String username;

    /**
     * 密码
     */
    @NotBlank(message = "密码不能为空")
//    @LimitLength(minLength = 3, maxLength = 10)
    private String password;

    /**
     * 邮件
     */
    @Email
    @NotBlank(message = "邮件不能为空")
    private String email;

    /**
     * 个性签名
     */
    private String sign;

    /**
     * 性别
     */
//    @ReturnData
    private String gender;

    /**
     * vip等级
     */
    private Integer vipLevel;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 状态
     */
    private Integer status;

    /**
     * 最后的登陆时间
     */
    private LocalDateTime lasted;

    /**
     * 创建日期
     */
    private LocalDateTime created;

    /**
     * 最后修改时间
     */
    private LocalDateTime modified;

    /*
        时间差（用于展示）
     */
    @TableField(exist = false)  //表示这是非数据库字段
    private String timeAgo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }
    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
    public Integer getVipLevel() {
        return vipLevel;
    }

    public void setVipLevel(Integer vipLevel) {
        this.vipLevel = vipLevel;
    }
    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
    public LocalDateTime getLasted() {
        return lasted;
    }

    public void setLasted(LocalDateTime lasted) {
        this.lasted = lasted;
    }
    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }
    public LocalDateTime getModified() {
        return modified;
    }

    public void setModified(LocalDateTime modified) {
        this.modified = modified;
    }
    public String getTimeAgo() {
        return timeAgo;
    }

    public void setTimeAgo(String timeAgo) {
        this.timeAgo = timeAgo;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "User{" +
            "id=" + id +
            ", username=" + username +
            ", password=" + password +
            ", email=" + email +
            ", sign=" + sign +
            ", gender=" + gender +
            ", vipLevel=" + vipLevel +
            ", avatar=" + avatar +
            ", status=" + status +
            ", lasted=" + lasted +
            ", created=" + created +
            ", modified=" + modified +
        "}";
    }
}
