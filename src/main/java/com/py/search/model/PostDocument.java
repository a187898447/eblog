package com.py.search.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(indexName = "post",createIndex = true)   //默认启动时创建
public class PostDocument implements Serializable {

    @Id
    private Long id;
    //                              搜索时使用（粗度划分）        插入时使用（细度划分）
    @Field(type = FieldType.Text,searchAnalyzer = "ik_smart",analyzer = "ik_max_word")
    private String title;

    @Field(type = FieldType.Long)
    private Long authorId;

    @Field(type = FieldType.Keyword)
    private String authorName;

    private String authorAvatar;

    private Long categoryId;

    @Field(type = FieldType.Keyword)
    private String categoryName;

    private Integer recommend;

    private Integer level;

    @Field(type = FieldType.Date,format = DateFormat.date_optional_time )
    private LocalDateTime created;

    private Integer commentCount;

    private String timeAgo;
}
