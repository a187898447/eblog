package com.py.search.repository;

import com.py.search.model.PostDocument;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;


@Repository //类似于@mapper    这里是将es作为类似mapper的形式使用，是jpa的写法，第一个参数指定了操作哪个索引
//个人觉得就是将CreateIndexRequest request = new CreateIndexRequest("xxx");的操作简化了
public interface PostRepository extends ElasticsearchRepository<PostDocument,Long> {

}
