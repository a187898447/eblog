package com.py.search.mq;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class WatchMessageImpl implements RabbitTemplate.ConfirmCallback , RabbitTemplate.ReturnCallback {
      @Autowired
      private RabbitTemplate template;

      @PostConstruct    //本注解会在依赖注入后执行  可以理解为一个初始化操作    这里本类实现了两个需要用到的接口并重写了他的方法所以可以直接用this
      public void initRabbitTemplate(){
          template.setConfirmCallback(this);
          template.setReturnCallback(this);
      }

    /**
     *  消息发送至交换机后回调
     * @param correlationData   相关的数据
     * @param ack   交换机接收消息是否成功
     * @param cause 如果么有成功，则返回原因
     */
    @Override
    public void confirm(CorrelationData correlationData, boolean ack, String cause) {
        if(ack){
            System.out.println("发送成功");
        }else{
            System.out.println("发送失败，原因是："+cause);
        }
    }
    /**
     *  消息未正常到达队列时回调
     * @param message   消息内容
     * @param replyCode 回调响应码
     * @param replyText 回调文本
     * @param exchange  交换机
     * @param routingKey    路由key
     */
    @Override
    public void returnedMessage(Message message, int replyCode, String replyText, String exchange, String routingKey) {
        System.out.println("消息未正常到达队列-replyCode="+replyCode);
        System.out.println("消息未正常到达队列-replyText="+replyText);
        System.out.println("消息未正常到达队列-exchange="+exchange);
        System.out.println("消息未正常到达队列-routingKey="+routingKey);
    }
}
