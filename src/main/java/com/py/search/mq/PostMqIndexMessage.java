package com.py.search.mq;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PostMqIndexMessage implements Serializable {
    //两种类型
    public final static String CREATE_OR_UPDATE = "created_update";
    public final static String REMOVE = "remove";


    private Long postId;

    //本次操作的类型
    private String type;
}
