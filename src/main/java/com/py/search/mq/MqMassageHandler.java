package com.py.search.mq;

import com.py.config.RabbitConfig;
import com.py.service.SearchService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RabbitListener(queues = RabbitConfig.QUEUE)
public class MqMassageHandler {
    @Autowired
    SearchService searchService;

    @RabbitHandler
    public void handler(PostMqIndexMessage message){

        log.info("mq收到一条消息:"+message.toString());

        switch (message.getType()) {
            case PostMqIndexMessage.CREATE_OR_UPDATE:
                searchService.createorupdate(message.getPostId());
                break;
            case PostMqIndexMessage.REMOVE:
                searchService.remove(message.getPostId());
                break;
        }
    }

}
