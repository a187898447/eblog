package com.py.util;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.py.vo.PostVo;

import java.time.Duration;
import java.time.LocalDateTime;

//计算时间差的帮助类
public final class TimeAgoUtil {

    private static final String ONE_MINUTE_AGO = "分钟前";
    private static final String ONE_HOUR_AGO = "小时前";
    private static final String ONE_DAY_AGO = "天前";
    private static final String ONE_UNKNOWN = "未知";

    //计算时间之差并添加到指定属性中
    public static String getTimeAgo(LocalDateTime created){
        Duration duration = Duration.between(created, LocalDateTime.now());  //获取时间差
        if(duration.toMinutes()<=2){
            return "刚刚";
        }else if(duration.toMinutes()<60 && duration.toMinutes()>2) {
            return duration.toMinutes()+ONE_MINUTE_AGO;
        }else if(duration.toHours()<24) {
            return duration.toHours()+ONE_HOUR_AGO;
        }else if(duration.toDays()<=30){
            return duration.toDays()+ONE_DAY_AGO;
        }else if(duration.toDays()>30) {
            return created.toString().replace("T"," ");
        }else{
            return ONE_UNKNOWN;
        }
    }
}
